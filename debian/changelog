r-cran-processx (3.8.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.1 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 27 Feb 2025 09:25:03 +0900

r-cran-processx (3.8.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 18 Jan 2025 15:00:47 +0100

r-cran-processx (3.8.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Mon, 17 Jun 2024 09:54:40 +0900

r-cran-processx (3.8.3-1) unstable; urgency=medium

  * Fix clean target
    Closes: #1047953
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 19 Dec 2023 17:53:54 +0100

r-cran-processx (3.8.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 21 Jul 2023 17:51:26 +0200

r-cran-processx (3.8.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Adapt Test-Depends to Suggests

 -- Andreas Tille <tille@debian.org>  Mon, 26 Jun 2023 15:00:50 +0200

r-cran-processx (3.8.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 02 Nov 2022 11:45:08 +0100

r-cran-processx (3.7.0-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jul 2022 09:15:00 +0200

r-cran-processx (3.6.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jun 2022 16:59:37 +0200

r-cran-processx (3.5.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Drop debian/patches/force-callr-versioned.patch,
      included in upstream release.

 -- Dylan Aïssi <daissi@debian.org>  Sat, 07 May 2022 10:30:55 +0200

r-cran-processx (3.5.2-2) unstable; urgency=medium

  * Team Upload.
  * [088a5f5] Revert "d/t/run-unit-test: Exclude one failing test"
  * [67b0d16] d/p/force-callr-versioned.patch: Version depend on
    new version of callr to get tests passing

 -- Nilesh Patra <nilesh@debian.org>  Fri, 24 Sep 2021 21:12:35 +0530

r-cran-processx (3.5.2-1) unstable; urgency=medium

  * Team Upload.
  [ Andreas Tille ]
  * New upstream version

  [ Nilesh Patra ]
  * d/t/run-unit-test: Exclude one failing test
  * Standards-Version: 4.6.0 (routine-update)

 -- Nilesh Patra <nilesh@debian.org>  Fri, 24 Sep 2021 02:58:35 +0530

r-cran-processx (3.4.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 05 Dec 2020 03:39:06 +0530

r-cran-processx (3.4.4-1) unstable; urgency=medium

  * New upstream version
  * Force UTF-8 in autopkgtest

 -- Andreas Tille <tille@debian.org>  Mon, 21 Sep 2020 11:46:02 +0200

r-cran-processx (3.4.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Thu, 16 Jul 2020 09:34:50 +0200

r-cran-processx (3.4.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit,
    Repository, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Wed, 19 Feb 2020 20:37:27 +0100

r-cran-processx (3.4.1-2) unstable; urgency=medium

  * Team upload.
  * Add r-cran-curl to Test-Depends.
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 03 Jan 2020 09:54:54 +0100

r-cran-processx (3.4.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.4.0

 -- Dylan Aïssi <daissi@debian.org>  Mon, 22 Jul 2019 08:19:26 +0200

r-cran-processx (3.4.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * rename debian/tests/control.autodep8 to debian/tests/control
  * Add r-cran-codetools to autopkgtest depends

 -- Andreas Tille <tille@debian.org>  Mon, 08 Jul 2019 08:53:05 +0200

r-cran-processx (3.2.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Mon, 10 Dec 2018 12:03:32 +0100

r-cran-processx (3.2.0-2) unstable; urgency=medium

  * Team upload
  * Add test dependency on r-cran-callr (Closes: #908757)

 -- Graham Inggs <ginggs@debian.org>  Thu, 06 Dec 2018 09:03:10 +0000

r-cran-processx (3.2.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.0
  * New Build-Depends: r-cran-ps

 -- Andreas Tille <tille@debian.org>  Tue, 11 Sep 2018 08:08:55 +0200

r-cran-processx (3.1.0-1) unstable; urgency=medium

  * Initial release (closes: #904982)

 -- Andreas Tille <tille@debian.org>  Mon, 30 Jul 2018 09:51:47 +0200
